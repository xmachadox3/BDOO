<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.conexion.BDOO"%>
<%@page import="com.db4o.ObjectContainer"%>
<%@page import="com.db4o.ObjectSet"%>

<%@page import="com.clases.CasaPlaya"%>
<%@page import="com.clases.Propietario"%>
<!DOCTYPE html>
<html lang="es">
	<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title>BDOO Jesus Machado</title>  
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>	
  	</head>
  	<body>
    	<div class="container">
      		<div class="header clearfix">
        		<nav class="navbar navbar-inverse navbar-fixed-top">
      				<div class="container">
        				<div class="navbar-header">
          					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            					<span class="sr-only">Toggle navigation</span>
            					<span class="icon-bar"></span>
            					<span class="icon-bar"></span>
            					<span class="icon-bar"></span>
          					</button>
          					<a class="navbar-brand" href="index.jsp">Casa de Playas</a>
        				</div>
        				<div id="navbar" class="navbar-collapse collapse">
        					<ul class="nav navbar-nav">
        						<li>
        							<a href="registrarse.jsp">
        								Registrarse        								
        							</a>
        						</li>
        						<li>
        							<a href="#">Link</a>
        						</li>
        					</ul>
                   <%boolean estado = false;	
                    		if(session.getAttribute("session") != null ){                     			
                    			String Usuario =  (String) session.getAttribute("session");
                    			estado = true;
                    		%>
                    		<ul class=" navbar-right">
        	        			<li style="color: #DDD; margin-top:5px; list-style-type: none;">
        							<div style="display: inline-block; font-weight: bold; margin-top: 10px;">
        							<%= Usuario %>
        							</div>
        							<div style="display: inline-block; margin-top: 5px; margin-left: 10px;">
        								<form method="post" action="DestruirSession">
        									<button type="submit" class="btn btn-sm btn-warning" name="cerrarSession" value="cerrarSession">
        										Cerrar Sesión
        									</button>
        								</form>
        							</div>
        							
        						</li>
        					</ul>
        				</div><!--/.navbar-collapse -->
      				</div>
    			</nav>
        	
      	</div>	
      	<div class="clearfix"></div>
      	<br>
      	<br>
      	<div class="container">
      	<div class="row">
      		<div class="col-lg-3">
      			<h2 class="text-center">Registrar Casas de Playa</h2>
      			<form method="post" action="RegistrarCasaPlaya" >
            		<div class="form-group">
              			<input type="text" placeholder="codigo" class="form-control" name ="codigo" required>
            		</div>	
            		<div class="form-group">
              			<input type="text" placeholder="poblacion" class="form-control" name= "poblacion" required>
            		</div>
            		<div class="form-group">
              			<input type="number" placeholder="Numeros de Baños" class="form-control" name= "nbanos" required>
            		</div>
            		<div class="form-group">
              			<input type="number" placeholder="Numeros de Cocinas" class="form-control" name= "ncocinas" required>
            		</div>
            		<div class="form-group">
              			<input type="number" placeholder="Numeros de Comedores" class="form-control" name= "ncomedores" required>
            		</div>
            		<div class="form-group">
              			<input type="number" placeholder="Numeros de Estacionamientos" class="form-control" name= "nestacionamientos" required>
            		</div>
            		<div class="form-group">
              			<input type="number" placeholder="Numeros de Habitaciones" class="form-control" name= "nhabitaciones" required>
            		</div>
            		<div class="form-group" style="display: none">
              			<input type="text"  class="form-control" name= "usuario" value="<%= Usuario%>" required>
            		</div>
            		<button type="submit" class="btn btn-success" name="registrarcasaplaya"  value="registrarcasaplaya">Guardar</button>
          		</form>
      			
      		</div>
      		<div class="col-lg-9">
      			<h2 class="text-center">Casas de Playa Registradas</h2> 
      			<table class="table table-hover">
      				<thead>
      					<th>Codigo</th>
      					<th>EstadoActual</th>
      					<th>#-Banos</th>
      					<th>#-Cocinas</th>
      					<th>#-Comedores</th>
      					<th>#-Estacionamientos</th>
      					<th>#-Habitaciones</th>
      					<th>#-Poblacion</th>
      					<th>#-Propietario</th>
      				</thead>
      				<%
      					try{
      					ObjectContainer db = BDOO.obtenerInstancia(); 
      					ObjectSet<Propietario> resultPropietario = db.queryByExample(new Propietario(null,null,null,null,null,Usuario,null,null));
      					Propietario x = null;
      					boolean flagsPropietario = false;
      					while(resultPropietario.hasNext()){
      						x = resultPropietario.next();
      						if(x.getLogin().equals(Usuario)){
      							flagsPropietario = true;
      							break;
      						}				
      					}
      					if(!flagsPropietario){
      						x = null;
      					}
      					
      					ObjectSet<CasaPlaya> resultCasaPlaya =  db.queryByExample(new CasaPlaya(null,null,null,null,null,null,null,true,null,x));
      					
      					CasaPlaya t = null;
      				%>
      				<tbody>
      				<%while(resultCasaPlaya.hasNext()){ 
      					t = resultCasaPlaya.next();
      				%>
      					<tr>
      						<td><%= t.getCodigo() %></td>
      						<td><%= t.isEstadoactual() %></td>
      						<td><%= t.getNbanos() %></td>
      						<td><%= t.getNcocinas() %></td>
      						<td><%= t.getNcomedores() %></td>
      						<td><%= t.getNestacionamientos() %></td>
      						<td><%= t.getNhabitaciones() %></td>
      						<td><%= t.getPoblacion()  %></td>
      						<td><%= t.getPropietario().getNombre() %></td>
      					</tr>
      				<%
      					}
      					
      				}catch(Exception e){
      					%><%="Error" %><%
      				}
      				%>
      				</tbody>
  				</table>
      		</div>     	
      	</div>
      	<br>
      	<div class="row">
      		<div class="col-lg-3">
      			<h2 class="text-center">Registrar Paquete Casa de Playa</h2>
      			<form method="post" action="RegistrarPaqueteCasaPlaya" >
            		<div class="form-group">
              			<input type="text" placeholder="Codigo de la Casa" class="form-control" name ="codigocasa" required>
            		</div>	
            		<div class="form-group">
              			<input type="text" placeholder="Codigo del Paquete" class="form-control" name= "codigopaquete" required>
            		</div>
            		<div class="form-group">
              			<input type="text" placeholder="Fecha Inicial" class="form-control" name= "finicial" required>
            		</div>
            		<div class="form-group">
              			<input type="text" placeholder="Fecha Final" class="form-control" name= "ffinal" required>
            		</div>
            		<div class="form-group">
              			<input type="number" placeholder="Precio" class="form-control" name= "precio" required>
            		</div>            		
            		<div class="form-group" style="display: none">
              			<input type="text"  class="form-control" name= "usuario" value="<%= Usuario%>" required>
            		</div>
            		<button type="submit" class="btn btn-success" name="registrarpaquetecasaplaya"  value="registrarcasaplaya">Guardar</button>
          		</form>
      		</div>
      	</div>
      	</div>
        					<%} else{ %>
        					<!--  
		 					<ul class=" navbar-right">
        	        			<li style="color: #DDD; margin-top:5px; list-style-type: none;">
        							<div style="display: inline-block; font-weight: bold; margin-top: 10px;">
        							</div>
        							<div style="display: inline-block; margin-top: 5px; margin-left: 10px;">
        								<form method="post" action="Dsession">
        									<button type="submit" class="btn btn-sm btn-warning" name="cerrarSession" value="cerrarSession">
        										Cerrar Sesión
        									</button>
        								</form>
        							</div>
        						</li>
        					</ul>   
        					-->	
        					
          					<form class="navbar-form navbar-right" method="post" action="Login" >
            					<div class="form-group">
              						<input type="text" placeholder="Login" class="form-control" name ="login">
            					</div>	
            					<div class="form-group">
              						<input type="password" placeholder="Clave" class="form-control" name= "password">
            					</div>
            					<button type="submit" class="btn btn-success" name="loginuser"  value="loginuser">Loguearse</button>
          					</form>
          				</div><!--/.navbar-collapse -->
      				</div>
    			</nav>
        	
      	</div>	
                  
    		<%} %>
    </div> <!-- /container -->
</html>

